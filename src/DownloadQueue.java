import java.util.ArrayList;

/**
 * This class is a Queue
 * DownloadQueue has a name
 * and a ArrayList of DownloadInfo
 *
 *
 * @author Alireza Mazochi
 * @since 1397-02-28
 * @version 1.0.0
 */
public class DownloadQueue {
    ArrayList<DownloadInfo> downloadInfos;
    String name;

    /**
     * Constructor for class DownloadQueue
     * @param name name of queue
     */
    public DownloadQueue(String name){
        this.name=name;
        downloadInfos=new ArrayList<>();
    }

    /**
     * a method for adding a new DownloadInfo to queue
     * @param downloadInfo new DownloadInfo
     */
    public void addDownload(DownloadInfo downloadInfo){
        downloadInfos.add(downloadInfo);
    }

    /**
     * Getter for downloadInfos
     * @return downloadInfos
     */
    public ArrayList<DownloadInfo> getDownloadInfos() {
        return downloadInfos;
    }

    /**
     * Getter for name
     * @return name
     */
    public String getName() {
        return name;
    }
}
