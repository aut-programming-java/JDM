import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;


/**
 * This class show QUI of a download
 * and for showing details needs to a object of class DownloadInfo
 *
 * @see DownloadInfo
 * @author Alireza Mazochi
 * @since 1397-02-28
 * @version 1.0.0
 */
public class DownloadInfoPanel extends JPanel{
    private JProgressBar progress;
    private JLabel name;
    private JLabel sizeLabel;
    private JLabel speedOfDownloadLabel;
    private JLabel progressPercentLabel;
    private JPanel progressPanel;
    private JPanel southPanel;

    private DownloadInfo downloadInfo;
    private MainFrame mainFrame;
    private DownloadManager downloadManager;

    private JPanel buttonsPanel;
    private ImageIcon playIcon;
    private ImageIcon removeIcon;
    private ImageIcon fileIcon;

    private JButton playIconButton;
    private JButton removeIconButton;
    private JButton fileIconButton;

    private boolean selected;

    /**
     * Constructor for DownloadInfoPanel
     * @param downloadInfo downloadInfo that set to this panel
     * @param mainFrame mainFrame of run application
     * @param downloadManager downloadManager
     */
    public DownloadInfoPanel (DownloadInfo downloadInfo,MainFrame mainFrame,DownloadManager downloadManager){
        this.downloadInfo=downloadInfo;
        this.mainFrame=mainFrame;
        this.downloadManager=downloadManager;

        setBorder(BorderFactory.createBevelBorder(0));
        if(downloadInfo.getName().length()>20){
            this.name=new JLabel(downloadInfo.getName().substring(0,20)+"...");
        }
        else{
            this.name=new JLabel(downloadInfo.getName());
        }
        sizeLabel=new JLabel(mainFrame.sizeMaker(downloadInfo.getSizeOfDownloaded())+
                " of "+mainFrame.sizeMaker(downloadInfo.getSizeOfFile()));

        speedOfDownloadLabel=new JLabel("0");

        progressPanel=new JPanel();
        progressPanel.setLayout(new BorderLayout(10,20));
        progress=new JProgressBar();
        progress.setMinimum(0);
        progress.setMaximum(100);
        progress.setValue(0);
        progress.setBackground(Color.WHITE);
        progress.setForeground(Color.blue);
        progressPercentLabel=new JLabel("0");
        progressPercentLabel.setSize(progressPercentLabel.getPreferredSize());
        progressPanel.add(progress,BorderLayout.CENTER);
        progressPanel.add(progressPercentLabel,BorderLayout.EAST);

        playIcon=new ImageIcon("play_download.png");
        removeIcon=new ImageIcon("remove_download.png");
        fileIcon=new ImageIcon("file_download.png");
        playIconButton=new JButton();
        removeIconButton=new JButton();
        fileIconButton=new JButton();
        playIconButton.setIcon(playIcon);
        removeIconButton.setIcon(removeIcon);
        fileIconButton.setIcon(fileIcon);
        playIconButton.setSize(20,20);
        removeIconButton.setSize(20,20);
        fileIconButton.setSize(20,20);

        buttonsPanel=new JPanel();
        buttonsPanel.setLayout(new GridLayout(1,3));
        buttonsPanel.setSize(75,25);
        buttonsPanel.add(sizeLabel);
        buttonsPanel.add(speedOfDownloadLabel);
        buttonsPanel.add(playIconButton);
        buttonsPanel.add(removeIconButton);
        buttonsPanel.add(fileIconButton);

        southPanel=new JPanel();
        southPanel.setLayout(new GridLayout(1,3,10,10));

        southPanel.add(buttonsPanel);
        southPanel.add(speedOfDownloadLabel);
        southPanel.add(sizeLabel);

        setSize(300,50);
        setLayout(new BorderLayout());
        add(name,BorderLayout.NORTH);
        add(progressPanel,BorderLayout.CENTER);
        add(southPanel,BorderLayout.SOUTH);

        MyMouseListener myMouseListener=new MyMouseListener();
        progressPanel.addMouseListener(myMouseListener);
        addMouseListener(myMouseListener);

        removeIconButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeThis();
            }
        });

        playIconButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                downloadManager.runDownload(downloadInfo);
                if(downloadInfo.getDownloadRun().isPause()){
                    downloadInfo.getDownloadRun().setPause(false);
                }
            }
        });

        fileIconButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop.getDesktop().open(new File(downloadInfo.getAddressOfFile()));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        selected=false;
        select();
    }

    /**
     * this method remove object of GUI
     */
    public void removeThis(){
        mainFrame.removeDownload(this);
    }

    /**
     * when downloadInfo changes downloadInfoPanel changes to.
     */
    public void update(){
        if(downloadInfo!=null) {
            speedOfDownloadLabel.setText(downloadInfo.getSpeedOfDownload() + " KB");
            progress.setValue(Integer.parseInt(downloadInfo.getProgressPercent()));
            progressPercentLabel.setText(downloadInfo.getProgressPercent());
            sizeLabel.setText(mainFrame.sizeMaker(downloadInfo.getSizeOfDownloaded()) +
                    " of " + mainFrame.sizeMaker(downloadInfo.getSizeOfFile()));
            speedOfDownloadLabel.setText(mainFrame.sizeMaker(downloadInfo.getSpeedOfDownload()));
            updateColor();
        }
        else{
            mainFrame.getShowingDownloadsInfoPanel().remove(this);
        }
    }

    /**
     * this method changes color when state of selecting changes
     */
    public void select(){
        if(selected){
            setBackground(Color.YELLOW);
            southPanel.setBackground(Color.YELLOW);
            buttonsPanel.setBackground(Color.YELLOW);
            progressPanel.setBackground(Color.YELLOW);
        }
        else{
            setBackground(null);
            southPanel.setBackground(null);
            buttonsPanel.setBackground(null);
            progressPanel.setBackground(null);
        }
    }

    /**
     * getter for selected
     * @return selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * setter for selected
     * @param selected
     */
    public void setSelected(boolean selected){
        this.selected=selected;
    }

    /**
     * getter for downloadInfo
     * @return downloadInfo
     */
    public DownloadInfo getDownloadInfo() {
        return downloadInfo;
    }

    /**
     * percent of progress set color of JProgressBar
     * This method does not work in Nimbus
     */
    public void updateColor(){
        if(progress.getValue()<20){
            progress.setForeground(Color.RED);
        }
        else if(progress.getValue()<40){
            progress.setForeground(Color.ORANGE);
        }
        else if(progress.getValue()<60){
            progress.setForeground(Color.yellow);
        }
        else if(progress.getValue()<80){
            progress.setForeground(Color.GREEN);
        }
        else{
            progress.setForeground(Color.blue);
        }
    }


    /**
     * This private class add many abilities to this class relate to show info,opening file,...
     */
    private class MyMouseListener extends MouseAdapter{


        /**
         * Invoked when the mouse button has been clicked (pressed
         * and released) on a component.
         *
         * @param e
         */
        @Override
        public void mouseClicked(MouseEvent e) {
            if(e.getButton()==MouseEvent.BUTTON3){
                mainFrame.showInfo(downloadInfo);
            }
            else if(e.getClickCount()== 2 && e.getButton()==MouseEvent.BUTTON1){
                if(downloadInfo.isCompleted()){
                    try {
                        Desktop.getDesktop().open(new File(downloadInfo.getAddressOfFile()+"/"+downloadInfo.getName()));
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(mainFrame,"File does not readable!","ERROR",JOptionPane.ERROR_MESSAGE);
                    }
                    catch (IllegalArgumentException e1){
                        JOptionPane.showMessageDialog(mainFrame,"File does not exist!","ERROR",JOptionPane.ERROR_MESSAGE);

                    }
                }
                else{
                    JOptionPane.showMessageDialog(mainFrame,"Download not completed at now!","ERROR",JOptionPane.ERROR_MESSAGE);
                }
            }
            else if(e.getClickCount()== 1 && e.getButton()==MouseEvent.BUTTON1){
                selected=!selected;
                select();
            }
        }
    }
}
