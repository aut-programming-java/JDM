import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

/**
 * This Frame contains method for change setting of application:
 * 1)Look and Feel
 * 2)Address of download location
 * 3)number of limited download
 * 4)Filter link
 *
 *
 * @author Alireza Mazochi
 * @since 1397-02-28
 * @version 1.0.0
 */
public class SettingFrame extends JFrame {
    private DownloadManager downloadManager;
    private MainFrame mainFrame;
    private ArrayList<String> filterAddresses;
    private String lookAndFeel="Metal";

    private PreferredButton removeFilter[];
    private JFrame filterFrame;

    /**
     * Constructor for SettingFrame
     * in this constructor make panel
     * and add action listener to buttons
     *
     * @param downloadManager DownloadManager for my run application
     * @param mainFrame mainFrame for my run application
     */
    public SettingFrame(DownloadManager downloadManager,MainFrame mainFrame){
        super("Setting");
        this.downloadManager=downloadManager;
        this.mainFrame=mainFrame;

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocation(500,100);
        JPanel contentPanel=new JPanel(new GridLayout(7,1,20,0));
        setContentPane(contentPanel);
        setVisible(true);
        readOfFile();

        String[] lookAndFeels={"Metal","Nimbus","Windows","Classic","Motif"};
        JComboBox comboBox=new JComboBox(lookAndFeels);
        comboBox.setSelectedItem(lookAndFeel);
        PreferredButton applyButton=new PreferredButton("Apply");
        PreferredButton filterAddressButton=new PreferredButton("Show Address Filter");
        //JFileChooser fileChooser=new JFileChooser("C:\\Desktop");

        PreferredButton openFileChooser=new PreferredButton("File Directory");
        JTextField numberDownloadText=new JTextField();
        numberDownloadText.setText(String.valueOf(downloadManager.getNumberOfDownload()));
        JLabel labelComboBox=new JLabel("Select your look_feel!");
        JLabel labelTextField=new JLabel("Select number of downloads!");

        applyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int inputNumber = Integer.parseInt(numberDownloadText.getText().toString());
                    if(0<=inputNumber && inputNumber<=100){
                        downloadManager.setNumberOfDownload(inputNumber);
                        System.out.println(downloadManager.getNumberOfDownload());
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Please enter a valid number!","Error",JOptionPane.WARNING_MESSAGE);
                    }
                }
                catch (NumberFormatException numberFormatException){
                    JOptionPane.showMessageDialog(null,"Please enter a valid number!","Error",JOptionPane.WARNING_MESSAGE);
                }

                lookAndFeel = comboBox.getSelectedItem().toString();
                changeLookAndFeel();
                writeOfFile();
                mainFrame.totalPaint();
                dispose();
            }

        });

        openFileChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser(downloadManager.getAddress());
                //  fileChooser.setCurrentDirectory(new java.io.File("."));
                fileChooser.setDialogTitle("File Chooser");
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setAcceptAllFileFilterUsed(false);


                if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    downloadManager.setAddress(fileChooser.getSelectedFile().toString());
                    writeOfFile();
//                    System.out.println("getCurrentDirectory(): " + fileChooser.getCurrentDirectory());
//                    System.out.println("getSelectedFile() : " + fileChooser.getSelectedFile());
                } else {
                    System.out.println("No Selection ");
                }
            }
        });

        filterAddressButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                filter();
            }
        });

        numberDownloadText.setSize(100,100);
        setSize(500,400);
        comboBox.setSize(100,50);
        numberDownloadText.setSize(100,50);
        labelComboBox.setSize(100,50);
        labelTextField.setSize(100,50);

        add(labelComboBox);
        add(comboBox);

        add(labelTextField);
        add(numberDownloadText);

        add(applyButton);

        add(openFileChooser);
        add(filterAddressButton);

//        for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
//            System.out.println(info.getClassName());
//        }

    }

    /**
     * Constructor for Setting
     * This constructor in act is a tool
     * for applying previous setting that store in setting.jdm
     * @param downloadManager downloadManager of program
     */
    public SettingFrame(DownloadManager downloadManager){
        super("Setting");
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("setting.jdm"));
            String str;
            str=reader.readLine();
            downloadManager.setNumberOfDownload(Integer.parseInt(str));
            str=reader.readLine();
            lookAndFeel=str;
            changeLookAndFeel();

        } catch (IOException ex) {
            System.out.println("setting.jdm not exist before;Apply default application setting");
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
        dispose();
    }

    /**
     * This method apply look and feel that chose before
     */
    public void changeLookAndFeel(){
        if(lookAndFeel.equals("Metal")){
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            }

        }
        else if(lookAndFeel.equals("Nimbus")){
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            }

        }
        else if(lookAndFeel.equals("Windows")){
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            }

        }
        else if(lookAndFeel.equals("Classic")){
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            }

        }
        else if(lookAndFeel.equals("Motif")){
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            }

        }
    }

    /**
     * This method make a frame
     * and does jobs about filtering
     */
    public void filter(){
        filterAddresses=new ArrayList<>();
        filterFrame=new JFrame("Filter Part");
        filterFrame.setLocationRelativeTo(null);


        readOfFileFilter();
        filterFrame.setVisible(true);
        filterFrame.setLayout(new GridLayout(filterAddresses.size()+2,1));

        removeFilter=new PreferredButton[filterAddresses.size()];
        JPanel filterPart[]=new JPanel[filterAddresses.size()];

        FilterHandler fileHandler=new FilterHandler();
        int cont=0;
        for(String string:filterAddresses){
            filterPart[cont]=new JPanel();
            filterPart[cont].setLayout(new BorderLayout());
            filterPart[cont].add(new JLabel(string),BorderLayout.CENTER);
            removeFilter[cont]=new PreferredButton("Remove");
            removeFilter[cont].addActionListener(fileHandler);
            filterPart[cont].add(removeFilter[cont],BorderLayout.EAST);
            filterFrame.add(filterPart[cont]);
            cont++;
        }

        PreferredButton addFilter=new PreferredButton("Add");
        JTextField textField=new JTextField();

        JPanel addFilterPart=new JPanel();
        addFilterPart.setLayout(new BorderLayout());
        addFilterPart.add(textField,BorderLayout.CENTER);
        addFilterPart.add(addFilter,BorderLayout.EAST);

        PreferredButton okButton=new PreferredButton("Ok");

        filterFrame.add(addFilterPart);
        filterFrame.add(okButton);
        filterFrame.setSize(500,filterFrame.getPreferredSize().height);

        addFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                filterAddresses.add(textField.getText());
                writeOfFileFilter();
                filterFrame.dispose();
                filter();

            }
        });

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                filterFrame.dispose();
            }
        });

    }

    /**
     * Reads name of filter web on filter.jdm
     */
    public void readOfFileFilter(){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("filter.jdm"));
            while (reader.ready()) {
                String line = reader.readLine();
                filterAddresses.add(line);
            }
        } catch (IOException ex) {
            System.out.println("setting.jdm not exist before;Apply default application setting");
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }

    /**
     * write name of filter web on filter.jdm
     */
    public void writeOfFileFilter(){
        File txtFile = new File("filter.jdm");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(txtFile));
            for(String string:filterAddresses){
                writer.write(string+"\r\n");
            }
            writer.flush();
        } catch (IOException ex) {
            System.out.println("setting.jdm not exist before;Apply default application setting");
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException ex) {
                ex.printStackTrace(System.err);
            }
        }
    }

    /**
     * In this part last setting will reads as setting.jdm
     *
     */
    public void readOfFile(){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("setting.jdm"));

            String str=reader.readLine();
            downloadManager.setNumberOfDownload(Integer.parseInt(str));

            str=reader.readLine();
            lookAndFeel=str;
            changeLookAndFeel();

            str=reader.readLine();
            downloadManager.setAddress(str);


        } catch (IOException ex) {
            System.out.println("setting.jdm not exist before;Apply default application setting");
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }

    /**
     * After avery change on setting write setting in setting.jdm
     */
    public void writeOfFile(){
        File txtFile = new File("setting.jdm");
        FileWriter writer=null;
        try {
            writer=new FileWriter(txtFile);
            writer.write(downloadManager.getNumberOfDownload()+"\r\n");

            writer.write(lookAndFeel);
            writer.write("\r\n");

            writer.write(downloadManager.getAddress()+"\r\n");

            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException ex) {
                ex.printStackTrace(System.err);
            }
        }
    }

    /**
     * This private class belongs to removeButton for filter web
     */
    private class FilterHandler implements ActionListener{

        /**
         * Invoked when an action occurs.
         *
         * @param e
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            for(int i=0 ; i<filterAddresses.size() ; i++){
                if(e.getSource().equals(removeFilter[i])){
                    filterAddresses.remove(i);
                    writeOfFileFilter();
                    filterFrame.dispose();
                    filter();

                }
            }
        }
    }

}
