import javax.swing.*;
/**
 * Task of this class is updating GUI in every time
 * Dates of Logic Part changed
 * And This is task of this class thst update this changed to GUI Part
 *
 * @author Alireza Mazochi
 * @since 1397-03-11
 * @version 1.0.0
 */
public class GUIUpdater extends SwingWorker<Void,Void> {
    private MainFrame mainFrame;
    private DownloadManager downloadManager;


    /**
     * Constructor for GUIUpdater
     * Every JDM had mainFrame and downloadManager
     * @param mainFrame mainFrame of program
     * @param downloadManager downloadManager of program
     */
    public GUIUpdater(MainFrame mainFrame,DownloadManager downloadManager) {
        this.mainFrame = mainFrame;
        this.downloadManager=downloadManager;
    }

    /**
     * Computes a result, or throws an exception if unable to do so.
     * <p>
     * <p>
     * Note that this method is executed only once.
     * <p>
     * <p>
     * Note: this method is executed in a background thread.
     *
     * @return the computed result
     * @throws Exception if unable to compute a result
     */
    @Override
    protected Void doInBackground() throws Exception {
        while (true) {
            try{
                mainFrame.updateShowingDownloadsPanel();
                mainFrame.updateInfoPanel();

                if (downloadManager.isChanged()){
                    mainFrame.updateChanel();
                    downloadManager.setChanged(false);
                }


            }
            catch (NullPointerException e){
            }

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {

            }
        }
    }



}
