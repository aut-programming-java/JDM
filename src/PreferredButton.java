import javax.swing.*;
import java.awt.*;

/**
 * This button is a special type of JButton
 * and sets size automatically
 *
 * @author Alireza Mazochi
 * @since 1397-02-28
 * @version 1.0.0
 */
public class PreferredButton extends JButton{

    /**
     * Constructor for PreferredButton
     * When button has not text in first
     */
    public PreferredButton(){
        super();
        int height=getPreferredSize().height+-10;
        int width=getPreferredSize().width-10;
        setSize(new Dimension(width,height));
    }

    /**
     * Constructor for PreferredButton
     * When button has primary text
     * @param text primary text
     */
    public PreferredButton(String text){
        super(text);
        int height=getPreferredSize().height+-10;
        int width=getPreferredSize().width-10;
        setSize(new Dimension(width,height));
    }
}
