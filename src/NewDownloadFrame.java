import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
/**
 * This frame provide a environment for request a download
 *
 * @author Alireza Mazochi
 * @since 1397-03-11
 * @version 1.0.0
 */
public class NewDownloadFrame extends JFrame {
    private JTextField link;
    private JRadioButton addWithoutQueue;
    private JRadioButton addInQueue;
    private ButtonGroup buttonGroup;
    private JButton buttonOK;
    private JButton buttonCancel;

    private DownloadManager downloadManager;
    private DownloadInfo newDownloadInfo;

    private String name;
    private long  sizeOfFile;
    private String addressOfFile;
    private String addressOfLink;

    private int state=4;

    /**
     * Constructor for NewDownloadFrame
     * @param downloadManager downloadManager of program
     */
    public NewDownloadFrame(DownloadManager downloadManager){

        super("New Download");
        JPanel contentPanel=new JPanel();
        setContentPane(contentPanel);
        contentPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        setLocation(200,50);
        setLayout(new GridLayout(5,1,10,10));
        setVisible(true);
        setResizable(false);
        this.downloadManager=downloadManager;
        setSize(900,300);

        link=new JTextField();
        link.setDisabledTextColor(Color.CYAN);
        JLabel labelURL=new JLabel("     URL  :  ");
        JPanel urlPanel=new JPanel(new BorderLayout());
        urlPanel.add(labelURL,BorderLayout.WEST);
        urlPanel.add(link,BorderLayout.CENTER);

        JPanel buttonPanel=new JPanel(new GridLayout(1,5));
        buttonOK=new JButton("OK");
        buttonOK.setSize(10,10);
        buttonCancel=new JButton("Cancel");
        buttonPanel.add(buttonOK);
        buttonPanel.add(buttonCancel);

        JPanel addressPanel=new JPanel(new BorderLayout());
        JLabel locationLabel=new JLabel("Location : ");
        JTextField addressText=new JTextField();
        addressText.setText(downloadManager.getAddress());
        addressText.setEditable(false);
        addressPanel.add(locationLabel,BorderLayout.WEST);
        addressPanel.add(addressText,BorderLayout.CENTER);


        addressOfFile=downloadManager.getAddress();

        addInQueue=new JRadioButton("Add in a queue");
        addWithoutQueue=new JRadioButton("Add",true);
        buttonGroup=new ButtonGroup();
        buttonGroup.add(addWithoutQueue);
        buttonGroup.add(addInQueue);

        add(urlPanel);
        add(addressPanel);
        add(addWithoutQueue);
        add(addInQueue);
        add(buttonPanel);

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                addressOfLink=link.getText();
//
//
//                DownloadUtil downloadUtil=new DownloadUtil();
//                if(downloadManager.isFilter(addressOfLink)){
//                    JOptionPane.showMessageDialog(null,"URL was filtered","ERROR",JOptionPane.ERROR_MESSAGE);
//
//                }
//                else{
//                    try {
//                        downloadUtil.downloadFile(addressOfLink);
//                        name=downloadUtil.getFileName();
//                        sizeOfFile=downloadUtil.getContentLength();
//                        downloadUtil.disconnect();
//                        if(sizeOfFile<0 || name==null){
//                            JOptionPane.showMessageDialog(null,"URL not valid","ERROR",JOptionPane.ERROR_MESSAGE);
//                        }
//                        else{
//                            validateDownloadFind();
//                        }
//
//                    } catch (IOException e1) {
//                        JOptionPane.showMessageDialog(null,"URL not valid","ERROR",JOptionPane.ERROR_MESSAGE);
//                    }
//
//                }
//
                switch (state){
                    case 0:
                        break;
                    case 1:
                        JOptionPane.showMessageDialog(null,"URL was filtered","ERROR",JOptionPane.ERROR_MESSAGE);
                        break;
                    case 2:
                        JOptionPane.showMessageDialog(null,"URL not valid","ERROR",JOptionPane.ERROR_MESSAGE);
                        break;
                    case 3:
                        validateDownloadFind();
                        break;
                    case 4:
                        JOptionPane.showMessageDialog(null,"URL not valid","ERROR",JOptionPane.ERROR_MESSAGE);
                        break;
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                dispose();
            }
        });

        link.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                changeState();
            }
            public void removeUpdate(DocumentEvent e) {
                changeState();
            }
            public void insertUpdate(DocumentEvent e) {
                changeState();
            }

        });
    }

    /**
     * when link changed we try to make a connection and when
     * user click Ok we done request fast
     */
    public void  changeState(){

        addressOfLink=link.getText();

        DownloadUtil downloadUtil=new DownloadUtil();
        if(downloadManager.isFilter(addressOfLink)){
            state=1;
        }
        else{
            try {
                downloadUtil.downloadFile(addressOfLink);
                name=downloadUtil.getFileName();
                sizeOfFile=downloadUtil.getContentLength();
                downloadUtil.disconnect();
                if(sizeOfFile<0 || name==null){
                    state=2;

                }
                else{
                    state=3;
                }

            } catch (IOException e1) {
                state=4;
            }
        }
    }

    /**
     * If we have fine a validate and not filtered we used this method
     */
    public void validateDownloadFind(){
        newDownloadInfo=new DownloadInfo(name,sizeOfFile,addressOfFile,addressOfLink,downloadManager);
        downloadManager.newDownload(newDownloadInfo,addInQueue.isSelected());
        setVisible(false);
        dispose();
    }


}
