import com.sun.webkit.ColorChooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class is main and basic of GUI part
 * whit this JFrame we can access to dates that store in logic part of application
 * And whit this class we can open other helpful Frame
 *
 *
 * @author Alireza Mazochi
 * @since 1397-02-28
 * @version 1.0.0
 */

public class MainFrame extends JFrame {
    private DownloadManager downloadManager;

    private JPanel toolbar;
    private ImageIcon newIcon;
    private ImageIcon cancelIcon;
    private ImageIcon resumeIcon;
    private ImageIcon removeIcon;
    private ImageIcon pauseIcon;
    private ImageIcon settingIcon;
    private ImageIcon applicationIcon;
    private ImageIcon sortIcon;

    private PreferredButton newButton;
    private PreferredButton cancelButton;
    private PreferredButton resumeButton;
    private PreferredButton removeButton;
    private PreferredButton pauseButton;
    private PreferredButton settingButton;
    private PreferredButton sortButton;

    private JMenuBar menuBar;
    private JMenu downloadMenu;
    private JMenu helpMenu;

    private JMenuItem newMenu;
    private JMenuItem cancelMenu;
    private JMenuItem resumeMenu;
    private JMenuItem removeMenu;
    private JMenuItem pauseMenu;
    private JMenuItem settingMenu;
    private JMenuItem exitMenu;
    private JMenuItem exportMenu;
    private JMenuItem aboutProgrammerMenu;
    private JMenuItem aboutAppMenu;

    private JScrollPane scrollPane;
    private JPanel downloadsPanel;

    private JPanel infoPanel;
    private JLabel infoName;
    private JLabel infoStatus;
    private JLabel infoProgressPercent;
    private JLabel infoSizeOfFile;
    private JLabel infoSizeOfDownload;
    private JLabel infoSpeed;
    private JLabel infoAddressFile;
    private JLabel infoAddressLink;
    private JLabel infoTimeOfStart;

    private JPanel queuePanel;
    private PreferredButton allDownloadQueueButton;
    private PreferredButton inQueueButton;
    private PreferredButton changeQueueButton;
    private PreferredButton completeQueueButton;
    private PreferredButton searchButton;

    private ImageIcon processingIcon;
    private ImageIcon queueIcon;
    private ImageIcon completedIcon;
    private ImageIcon queueSettingIcon;
    private ImageIcon searchIcon;

    private ArrayList<DownloadInfoPanel> showingDownloadsInfoPanel;
    private String searchString;

    private DownloadInfo downloadInfoInInfoPanel;

    private char downloadPartChanel;
    //'P' --> Process
    //'Q' --> Queue
    //'C' --> Completed
    //'S' --> Search

    /**
     * Constructor for MainFrame
     * @param downloadManager downloadManager of program
     */
    public MainFrame(DownloadManager downloadManager){
        super("JDM");
        this.downloadManager=downloadManager;
        totalPaint();
    }

    /**
     * This method is a part of constructor
     * and use when look and feel changes
     */
    public void totalPaint(){
        applyPreviousSetting();
        JPanel contentPanel=new JPanel();
        setContentPane(contentPanel);
        setLocation(100,100);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(1100,530);
        setBackground(Color.lightGray);
        contentPanel.setBorder(BorderFactory.createLineBorder(Color.gray,5));

        applicationIcon=new ImageIcon("galaxy.png");
        setIconImage(applicationIcon.getImage());
        downloadInfoInInfoPanel=null;
        downloadPartChanel=0;

        makeInfoPanel();
        makeDownloadPart();
        makeToolBar();
        makeMenuBar();
        makeSystemTray();
        makeQueueBar();

        if(downloadManager!=null){
            downloadManager.sort();
            updateDownloads();
        }

        setJMenuBar(menuBar);
        JLabel logoLabel=new JLabel();
        ImageIcon logoIcon=new ImageIcon("logo.png");
        logoLabel.setIcon(logoIcon);

        JPanel leftPanel=new JPanel();
        leftPanel.setLayout(new BorderLayout());


        leftPanel.add(logoLabel,BorderLayout.NORTH);
        leftPanel.add(queuePanel,BorderLayout.CENTER);
        add(leftPanel,BorderLayout.WEST);

        JPanel mainPanel=new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(infoPanel,BorderLayout.EAST);
        mainPanel.add(toolbar,BorderLayout.NORTH);
        mainPanel.add(scrollPane,BorderLayout.CENTER);
        add(mainPanel,BorderLayout.CENTER);

        leftPanel.setBackground(new Color(50,54,63));
        queuePanel.setBackground(new Color(50,54,63));


    }

    /**
     * This method prepare SystemTray of application
     * and setting about this part
     */
    public void makeSystemTray(){
        SystemTray tray = SystemTray.getSystemTray();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image image = toolkit.getImage("galaxy.png");
        TrayIcon trayIcon=new TrayIcon(image);
        trayIcon.setImageAutoSize(true);
        trayIcon.setToolTip("JDM");
        trayIcon.addMouseListener(new MouseAdapter() {
            /**
             * {@inheritDoc}
             *
             * @param e
             */
            @Override
            public void mouseClicked(MouseEvent e) {
                setVisible(true);

            }
        });

        addWindowListener(new WindowAdapter() {
            /**
             * Invoked when a window is in the process of being closed.
             * The close operation can be overridden at this point.
             *
             * @param e
             */
            @Override
            public void windowClosing(WindowEvent e) {
                setVisible(false);
                try {
                    tray.add(trayIcon);
                } catch (AWTException e1) {
                    e1.printStackTrace();
                }
            }

            /**
             * Invoked when a window is activated.
             *
             * @param e
             */
            @Override
            public void windowActivated(WindowEvent e) {
                setVisible(true);
                tray.remove(trayIcon);
            }
        });
    }

    /**
     * Shows GUI to user
     */
    public void showGUT(){
//        pack();
        setVisible(true);
    }

    /**
     * Makes menu and items of it and actionListener
     */
    public void makeMenuBar(){
        menuBar=new JMenuBar();
        downloadMenu=new JMenu("Download");
        helpMenu=new JMenu("Help");
        newMenu=new JMenuItem("New Download");
        pauseMenu=new JMenuItem("Pause");
        resumeMenu=new JMenuItem("Resume");
        cancelMenu=new JMenuItem("Cancel");
        removeMenu=new JMenuItem("Remove");
        settingMenu=new JMenuItem("Setting");
        exportMenu=new JMenuItem("Export");
        exitMenu=new JMenuItem("Exit");
        aboutProgrammerMenu=new JMenuItem("About Programmer");
        aboutAppMenu=new JMenuItem("About Application");

        downloadMenu.setMnemonic('D');
        helpMenu.setMnemonic('H');
        newMenu.setMnemonic('N');
        pauseMenu.setMnemonic('P');
        resumeMenu.setMnemonic('R');
        cancelMenu.setMnemonic('C');
        removeMenu.setMnemonic('M');
        settingMenu.setMnemonic('S');
        exportMenu.setMnemonic('O');
        exitMenu.setMnemonic('E');

        aboutProgrammerMenu.setMnemonic('B');
        aboutAppMenu.setMnemonic('A');

        newMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Event.CTRL_MASK));
        pauseMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,Event.CTRL_MASK));
        resumeMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,Event.CTRL_MASK));
        cancelMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,Event.CTRL_MASK));
        removeMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M,Event.CTRL_MASK));
        settingMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,Event.CTRL_MASK));
        exportMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,Event.CTRL_MASK));
        exitMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,Event.CTRL_MASK));
        aboutProgrammerMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,Event.CTRL_MASK));
        aboutAppMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,Event.CTRL_MASK));

        ActionHandler actionHandler=new ActionHandler();
        newMenu.addActionListener(actionHandler);
        pauseMenu.addActionListener(actionHandler);
        resumeMenu.addActionListener(actionHandler);
        removeMenu.addActionListener(actionHandler);
        cancelMenu.addActionListener(actionHandler);
        settingMenu.addActionListener(actionHandler);
        exitMenu.addActionListener(actionHandler);
        exportMenu.addActionListener(actionHandler);
        aboutProgrammerMenu.addActionListener(actionHandler);
        aboutAppMenu.addActionListener(actionHandler);

        menuBar.add(downloadMenu);
        menuBar.add(helpMenu);
        downloadMenu.add(newMenu);
        downloadMenu.add(pauseMenu);
        downloadMenu.add(resumeMenu);
        downloadMenu.add(cancelMenu);
        downloadMenu.add(removeMenu);
        downloadMenu.add(settingMenu);
        downloadMenu.add(exportMenu);
        downloadMenu.add(exitMenu);
        helpMenu.add(aboutProgrammerMenu);
        helpMenu.add(aboutAppMenu);

    }

    /**
     * Makes toolBar Panel and buttons
     */
    public void makeToolBar(){
        toolbar=new JPanel();
        toolbar.setLayout(new GridLayout(1,7));

        newIcon=new ImageIcon("new.png");
        cancelIcon=new ImageIcon("cancel.png");
        resumeIcon=new ImageIcon("resume.png");
        removeIcon=new ImageIcon("remove.png");
        pauseIcon=new ImageIcon("pause.png");
        settingIcon=new ImageIcon("setting.png");
        sortIcon=new ImageIcon("sort.png");

        newButton=new PreferredButton();
        cancelButton=new PreferredButton();
        resumeButton=new PreferredButton();
        removeButton=new PreferredButton();
        pauseButton=new PreferredButton();
        settingButton=new PreferredButton();
        sortButton=new PreferredButton();

        newButton.setToolTipText("New");
        cancelButton.setToolTipText("Cancel");
        resumeButton.setToolTipText("Resume");
        removeButton.setToolTipText("Remove");
        pauseButton.setToolTipText("Pause");
        settingButton.setToolTipText("Setting");
        sortButton.setToolTipText("Sort by");

        newButton.setIcon(newIcon);
        cancelButton.setIcon(cancelIcon);
        resumeButton.setIcon(resumeIcon);
        removeButton.setIcon(removeIcon);
        pauseButton.setIcon(pauseIcon);
        settingButton.setIcon(settingIcon);
        sortButton.setIcon(sortIcon);

        ActionHandler actionHandle=new ActionHandler();
        newButton.addActionListener(actionHandle);
        cancelButton.addActionListener(actionHandle);
        resumeButton.addActionListener(actionHandle);
        removeButton.addActionListener(actionHandle);
        pauseButton.addActionListener(actionHandle);
        settingButton.addActionListener(actionHandle);
        sortButton.addActionListener(actionHandle);

        toolbar.add(newButton);
        toolbar.add(pauseButton);
        toolbar.add(resumeButton);
        toolbar.add(cancelButton);
        toolbar.add(sortButton);
        toolbar.add(removeButton);
        toolbar.add(settingButton);
    }

    /**
     * Makes panel that uses for show queue and search
     */
    public void makeQueueBar(){
        queuePanel=new JPanel(new GridLayout(8,1));
        allDownloadQueueButton=new PreferredButton("Processing");
        inQueueButton=new PreferredButton("Queue");
        changeQueueButton=new PreferredButton("Change Queue");
        completeQueueButton=new PreferredButton("Completed");
        searchButton=new PreferredButton("Search");

        processingIcon=new ImageIcon("processing.png");
        completedIcon=new ImageIcon("completed.png");
        queueIcon=new ImageIcon("queue.png");
        queueSettingIcon=new ImageIcon("setting2.png");
        searchIcon=new ImageIcon("search.png");

        allDownloadQueueButton.setIcon(processingIcon);
        inQueueButton.setIcon(queueIcon);
        completeQueueButton.setIcon(completedIcon);
        changeQueueButton.setIcon(queueSettingIcon);
        searchButton.setIcon(searchIcon);

        allDownloadQueueButton.setIconTextGap(15);
        inQueueButton.setIconTextGap(30);
        completeQueueButton.setIconTextGap(20);
        searchButton.setIconTextGap(30);

        queuePanel.add(allDownloadQueueButton);
        queuePanel.add(inQueueButton);
        queuePanel.add(changeQueueButton);
        queuePanel.add(completeQueueButton);
        queuePanel.add(searchButton);

        allDownloadQueueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDownloads();
                downloadManager.writeDownloadsFile();
            }
        });

        inQueueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDownloadsInQueue();
                downloadManager.writeDownloadsFile();

            }
        });

        MainFrame mainFrame=this;
        changeQueueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(downloadManager.getDownloads().size()==0){
                    JOptionPane.showMessageDialog(null,
                            "There is not any download!","ERROR",JOptionPane.ERROR_MESSAGE);
                }
                else{
                    QueueFrame queueFrame=new QueueFrame(downloadManager,mainFrame);
                }
            }
        });

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                search();
            }
        });

        completeQueueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDownloadsCompleted();
                downloadManager.writeDownloadsFile();
            }
        });
    }

    /**
     * This method for searching in name and link of downloads
     */
    public void search(){
        String searchString;
        searchString=JOptionPane.showInputDialog("Input your word");
        do{
            if(searchString.trim().isEmpty()==false){
                downloadPartChanel='S';
                this.searchString=searchString;
                updateSearch();

                break;
            }
        }while(true);

    }

    /**
     * Export a zip file of jdm file
     */
    public void exporting(){
            ExportZip exportZip=new ExportZip();
            exportZip.makeZip();
    }

    /**
     * This method calls for applying setting that has wrote in setting.jdm
     */
    public void applyPreviousSetting(){
        SettingFrame settingFrame=new SettingFrame(downloadManager);
    }

    /**
     * Makes a part for showing DownloadInfo
     */
    public void makeDownloadPart(){
        downloadsPanel=new JPanel(new GridLayout(0,1));
        scrollPane = new JScrollPane(downloadsPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    }

    /**
     * When New download is request
     */
    public void newDownload(){
        downloadManager.readFileFilter();
        NewDownloadFrame newDownloadFrame=new NewDownloadFrame(downloadManager);

    }

    /**
     * When cancel download is request
     */
    public void cancelDownload(){
        for(DownloadInfoPanel downloadInfoPanel:showingDownloadsInfoPanel){
            if(downloadInfoPanel.isSelected()){
                downloadInfoPanel.getDownloadInfo().getDownloadRun().cancel(true);
            }
        }
        selectingFalse();
    }

    /**
     * This application had many chanel:Process,Queue,Search and completed
     * This method remake this chanel
     */
    public void updateChanel(){
        switch (downloadPartChanel){
            case 'P':
                updateDownloads();
                break;
            case 'C':
                updateDownloadsCompleted();
                break;
            case 'Q':
                updateDownloadsInQueue();
                break;
            case 'S':
                updateSearch();
                break;
        }
    }

    /**
     * This method for showing downloads that choose with searchString
     */
    public void updateSearch(){
        ArrayList<DownloadInfo> searchDownloadInfo=new ArrayList<>();
        for(DownloadInfo downloadInfo:downloadManager.getDownloads()){
            if(downloadInfo.getName().contains(searchString) || downloadInfo.getAddressOfLink().contains(searchString)){
                searchDownloadInfo.add(downloadInfo);
            }
        }
        showingDownloadsInfoPanel=new ArrayList<>();
        for (DownloadInfo downloadInfo : searchDownloadInfo){
            DownloadInfoPanel downloadInfoPanel = new DownloadInfoPanel(downloadInfo, this,downloadManager);
            showingDownloadsInfoPanel.add(downloadInfoPanel);
        }

        reShowDownloadPanel();
    }


    /**
     * This method for showing downloads in queue
     */
    public void updateDownloadsInQueue() {
        downloadPartChanel='Q';
        showingDownloadsInfoPanel=new ArrayList<>();

        for (DownloadInfo downloadInfo : downloadManager.getQueue().getDownloadInfos()) {
                DownloadInfoPanel downloadInfoPanel = new DownloadInfoPanel(downloadInfo, this,downloadManager);
                showingDownloadsInfoPanel.add(downloadInfoPanel);
        }

        reShowDownloadPanel();
    }

    /**
     * This method for showing downloads completed
     */
    public void updateDownloadsCompleted(){
        downloadPartChanel='C';
        showingDownloadsInfoPanel=new ArrayList<>();

        for(DownloadInfo downloadInfo:downloadManager.getCompleted()){
            DownloadInfoPanel downloadInfoPanel=new DownloadInfoPanel(downloadInfo,this,downloadManager);
            showingDownloadsInfoPanel.add(downloadInfoPanel);
        }

        reShowDownloadPanel();
    }

    /**
     * This method for showing all downloads
     */
    public void updateDownloads(){
        downloadPartChanel='P';
        showingDownloadsInfoPanel=new ArrayList<>();
        for(DownloadInfo downloadInfo:downloadManager.getDownloads()){
            if(downloadInfo.getName()==null){
                downloadManager.removeDownload(downloadInfo);
                updateDownloads();
            }
            else{
                DownloadInfoPanel downloadInfoPanel=new DownloadInfoPanel(downloadInfo,this,downloadManager);
                showingDownloadsInfoPanel.add(downloadInfoPanel);
            }
        }
        reShowDownloadPanel();
    }

    /**
     * This method turn off all selected DownloadInfoPanel
     */
    public void selectingFalse(){
        for(DownloadInfoPanel downloadInfoPanel:showingDownloadsInfoPanel){
            downloadInfoPanel.setSelected(false);
            downloadInfoPanel.select();
        }
    }

    /**
     * this method update downloadInfoPanels
     */
    public void updateShowingDownloadsPanel(){
        for(DownloadInfoPanel downloadInfoPanel:showingDownloadsInfoPanel){
            downloadInfoPanel.update();
        }
    }

    /**
     * this method make downloadsPanel and set showingDownloadsInfoPanel
     */
    public void reShowDownloadPanel(){
        downloadsPanel.removeAll();
        if(showingDownloadsInfoPanel.size()>5){
            downloadsPanel.setLayout(new GridLayout(showingDownloadsInfoPanel.size(),1));
        }
        else{
            downloadsPanel.setLayout(new GridLayout(5,1));
        }

        for(DownloadInfoPanel downloadInfoPanel:showingDownloadsInfoPanel){
            downloadInfoPanel.update();
            downloadsPanel.add(downloadInfoPanel);
        }
        revalidate();
        repaint();

    }

    /**
     * When resume download is request
     */
    public void resumeDownload(){
        for(DownloadInfoPanel downloadInfoPanel:showingDownloadsInfoPanel){
            if(downloadInfoPanel.isSelected()){
                downloadInfoPanel.getDownloadInfo().getDownloadRun().resume();
            }
        }
        selectingFalse();
    }

    /**
     * When remove download is request
     */
    public void removeDownload(){
        for(DownloadInfoPanel downloadInfoPanel:showingDownloadsInfoPanel){
            if(downloadInfoPanel.isSelected()){
                downloadInfoPanel.getDownloadInfo().getDownloadRun().cancel(true);
                downloadManager.removeDownload(downloadInfoPanel.getDownloadInfo());
                showingDownloadsInfoPanel.remove(downloadInfoPanel);
                break;
            }
        }
        selectingFalse();
        reShowDownloadPanel();
    }

    /**
     * When remove download is request by downloadInfoPanel
     * @param downloadInfoPanel
     */
    public void removeDownload(DownloadInfoPanel downloadInfoPanel){
        downloadInfoPanel.getDownloadInfo().getDownloadRun().cancel(true);
        downloadManager.removeDownload(downloadInfoPanel.getDownloadInfo());
        showingDownloadsInfoPanel.remove(downloadInfoPanel);
        selectingFalse();
        reShowDownloadPanel();
    }

    /**
     * When pause download is request
     */
    public void pauseDownload(){
        for(DownloadInfoPanel downloadInfoPanel:showingDownloadsInfoPanel){
            if(downloadInfoPanel.isSelected()){
                downloadInfoPanel.getDownloadInfo().getDownloadRun().setPause(true);
            }
        }
        selectingFalse();
    }

    /**
     * This method run a SettingFrame for changing setting of program
     */
    public void setting(){
        SettingFrame settingFrame=new SettingFrame(downloadManager,this);
    }

    /**
     * This method is end of a program
     * and write last state of downloads in file for next time
     */
    public void exit(){
        downloadManager.writeDownloadsFile();
        System.exit(0);
    }

    /**
     *This method make a environment for change type of sorting
     */
    public void sort(){
        JFrame sortFrame=new JFrame("Sort");
        sortFrame.setLayout(new BorderLayout());
        sortFrame.setLocationRelativeTo(null);

        JPanel leftPanel;
        JPanel rightPanel;
        JCheckBox sortCheckTime;
        JCheckBox sortCheckName;
        JCheckBox sortCheckSize;
        JRadioButton sortIncrease;
        JRadioButton sortDecrease;
        PreferredButton applySortButton;

        applySortButton=new PreferredButton("Sort");

        sortCheckName=new JCheckBox("Name");
        sortCheckTime=new JCheckBox("Time");
        sortCheckSize=new JCheckBox("Size");

        sortCheckTime.setSelected(downloadManager.isDownloadSortTime());
        sortCheckName.setSelected(downloadManager.isDownloadSortName());
        sortCheckSize.setSelected(downloadManager.isDownloadSortSize());

        leftPanel=new JPanel();
        rightPanel=new JPanel();

        leftPanel.setBorder(BorderFactory.createLineBorder(Color.black,4));
        rightPanel.setBorder(BorderFactory.createLineBorder(Color.black,4));

        leftPanel.setLayout(new GridLayout(4,1,10,0));
        leftPanel.add(new JLabel("Sort by:"));
        leftPanel.add(sortCheckTime);
        leftPanel.add(sortCheckName);
        leftPanel.add(sortCheckSize);

        ButtonGroup buttonGroup=new ButtonGroup();
        sortIncrease= new JRadioButton("Increase");
        sortDecrease=new JRadioButton("Decrease");

        if(downloadManager.isDownloadSortIncrease()){
            sortIncrease.setSelected(true);
            sortDecrease.setSelected(false);
        }
        else{
            sortIncrease.setSelected(false);
            sortDecrease.setSelected(true);
        }
        buttonGroup.add(sortIncrease);
        buttonGroup.add(sortDecrease);
        rightPanel.setLayout(new GridLayout(3,1,10,0));
        rightPanel.add(new JLabel("Type of sort:"));
        rightPanel.add(sortIncrease);
        rightPanel.add(sortDecrease);

        sortFrame.add(leftPanel,BorderLayout.WEST);
        sortFrame.add(rightPanel,BorderLayout.EAST);
        sortFrame.add(applySortButton,BorderLayout.SOUTH);

        sortFrame.setVisible(true);
        sortFrame.setSize(170,200);

        applySortButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                downloadManager.sort(sortCheckTime.isSelected(),sortCheckName.isSelected(),sortCheckSize.isSelected()
                        ,sortIncrease.isSelected());
                updateDownloads();
                sortFrame.dispose();
            }
        });
    }

    /**
     * This part show info about a selected DownloadInfoPanel
     * And manages with DownloadInfoPanels
     */
    public void makeInfoPanel(){
        infoPanel=new JPanel(new GridLayout(9,2));

        infoName=new JLabel();
        infoStatus=new JLabel();
        infoProgressPercent=new JLabel();
        infoSizeOfFile=new JLabel();
        infoSizeOfDownload=new JLabel();
        infoSpeed=new JLabel();
        infoAddressFile=new JLabel();
        infoAddressLink=new JLabel();
        infoTimeOfStart=new JLabel();

        infoPanel.add(new JLabel("Name: "));
        infoPanel.add(infoName);
        infoPanel.add(new JLabel("Status:  "));
        infoPanel.add(infoStatus);
        infoPanel.add(new JLabel("Percent Of Progress: "));
        infoPanel.add(infoProgressPercent);
        infoPanel.add(new JLabel("Size of Downloaded: "));
        infoPanel.add(infoSizeOfDownload);
        infoPanel.add(new JLabel("Size of File: "));
        infoPanel.add(infoSizeOfFile);
        infoPanel.add(new JLabel("Speed of Download: "));
        infoPanel.add(infoSpeed);
        infoPanel.add(new JLabel("Created: "));
        infoPanel.add(infoTimeOfStart);
        infoPanel.add(new JLabel("Address of file: "));
        infoPanel.add(infoAddressFile);
        infoPanel.add(new JLabel("Link Of download: "));
        infoPanel.add(infoAddressLink);

    }

    /**
     * This method update dates of infoPanel
     */
    public void updateInfoPanel(){
        if(downloadInfoInInfoPanel!=null){
            DownloadInfo downloadInfo=downloadInfoInInfoPanel;
            infoProgressPercent.setText(downloadInfo.getProgressPercent());
            infoSizeOfFile.setText(sizeMaker(downloadInfo.getSizeOfFile()));
            infoSizeOfDownload.setText(sizeMaker(downloadInfo.getSizeOfDownloaded()));
            infoTimeOfStart.setText(downloadInfo.getTimeOfStart());

            if(downloadInfo.getDownloadRun().isPause()==false || downloadInfo.isCompleted()==false){
                infoSpeed.setText(sizeMaker(downloadInfo.getSpeedOfDownload()));
            }
            else{
                infoSpeed.setText("0 B");
            }
            if(downloadInfo.isCompleted()){
                infoStatus.setText("Completed");
            }
            else if(downloadInfo.getDownloadRun().isCancelled()){
                infoStatus.setText("Canceled");
            }
            else if(downloadInfo.getDownloadRun().isPause()){
                infoStatus.setText("Pause");
            }
            else{
                infoStatus.setText("Running");
            }

            if(downloadInfo.getAddressOfLink().length()>20){
                infoAddressLink.setText(downloadInfo.getAddressOfLink().substring(0,20)+"...");
            }
            else{
                infoAddressLink.setText(downloadInfo.getAddressOfLink());
            }

            if(downloadInfo.getAddressOfFile().length()>20){
                infoAddressFile.setText(downloadInfo.getAddressOfFile().substring(0,20)+"...");
            }
            else{
                infoAddressFile.setText(downloadInfo.getAddressOfFile());
            }

            if(downloadInfo.getName().length()>20){
                infoName.setText(downloadInfo.getName().substring(0,20)+"...");
            }
            else{
                infoName.setText(downloadInfo.getName());
            }

        }

    }

    /**
     * getter for showingDownloadsInfoPanel
     * @return showingDownloadsInfoPanel
     */
    public ArrayList<DownloadInfoPanel> getShowingDownloadsInfoPanel() {
        return showingDownloadsInfoPanel;
    }

    /**
     * Gives a downloadInfo and sets to components of InfoPanel
     * @param downloadInfo
     */
    public void showInfo(DownloadInfo downloadInfo){
        downloadInfoInInfoPanel=downloadInfo;

    }

    /**
     * This method make num of byte readable for human
     * @param numOfBytes
     * @return a string that is readable
     */
    public String sizeMaker(long numOfBytes){
        if(numOfBytes<1024){
            return numOfBytes+" B";
        }
        if(numOfBytes<1024*1024){
            float out=Float.valueOf(numOfBytes)/1024;
            return String.format("%.2f",out)+" KB";
        }
        if(numOfBytes<1024*1024*1024){
            float out=(Float.valueOf(numOfBytes)/1024/1024);
            return (String.format("%.2f",out))+" MB";
        }
        else{
            float out=(Float.valueOf(numOfBytes)/1024/1024/1024);
            return (String.format("%.2f",out))+" GB";
        }
    }

    /**
     * This action listener contains action listener of many components like button in toolbar
     * or itemMenu
     */
    private class ActionHandler implements ActionListener {
        /**
         * Invoked when an action occurs.
         *
         * @param e
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource().equals(aboutProgrammerMenu)){
                JOptionPane.showMessageDialog(null,
                        "Name : Alireza Mazochi\n" + "Student Number : 9631064\n"+"Start of Project : 1396/2/6\n"
                        +"End Of Project: 1396/2/14","About Programmer",JOptionPane.INFORMATION_MESSAGE);
            }

            if(e.getSource().equals(aboutAppMenu)){
                JOptionPane.showMessageDialog(null,
                        "This application for managing your downloads\n","About App"
                        ,JOptionPane.INFORMATION_MESSAGE);

            }
            
            if(e.getSource().equals(newMenu) || e.getSource().equals(newButton)){
                newDownload();
            }

            if(e.getSource().equals(cancelMenu)||e.getSource().equals(cancelButton)){
                cancelDownload();
            }

            if(e.getSource().equals(resumeMenu)||e.getSource().equals(resumeButton)){
                resumeDownload();
            }

            if(e.getSource().equals(removeMenu)||e.getSource().equals(removeButton)){
                removeDownload();
            }

            if(e.getSource().equals(pauseMenu)||e.getSource().equals(pauseButton)){
                pauseDownload();
            }

            if(e.getSource().equals(settingMenu)||e.getSource().equals(settingButton)){
                setting();
            }

            if(e.getSource().equals(exportMenu)){
                exporting();
            }

            if(e.getSource().equals(exitMenu)){
                exit();
            }

            if(e.getSource().equals(sortButton)){
                sort();
            }
        }
    }


}
