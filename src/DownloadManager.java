import java.io.*;
import java.util.ArrayList;

/**
 * This class used for managing download
 * and stores all download
 * and keep queue
 *
 *
 * @author Alireza Mazochi
 * @since 1397-02-28
 * @version 1.0.0
 */

public class DownloadManager {
    private int numberOfDownload=100;
    private String address="C:\\Users\\salam\\Desktop";
    private DownloadQueue queue;
    private ArrayList<DownloadInfo> downloads;
    private ArrayList<DownloadInfo> withoutQueue;
    private ArrayList<DownloadInfo> completed;
    private ArrayList<DownloadInfo> limited;
    private ArrayList<Boolean> limitedStateQueue;

    private boolean downloadSortTime;
    private boolean downloadSortName;
    private boolean downloadSortSize;
    private boolean downloadSortIncrease;

    private boolean changed;

    private ArrayList<String> filterLink;

    /**
     * Constructor for DownloadManager
     * in this part set values for sort downloads
     * and read information about previous download and queue(if exists)
     */
    public DownloadManager(){
        queue=new DownloadQueue("Queue");
        downloads=new ArrayList<>();
        withoutQueue=new ArrayList<>();
        completed=new ArrayList<>();
        limited=new ArrayList<>();
        limitedStateQueue=new ArrayList<>();

        downloadSortTime=true;
        downloadSortName=false;
        downloadSortSize=false;
        downloadSortIncrease=false;

        filterLink=new ArrayList<>();
        readDownloadFile();
        readFileFilter();

        for(DownloadInfo downloadInfo:withoutQueue){
            downloadInfo.getDownloadRun().execute();
        }
        queueManage();
    }

    /**
     * Setter for downloadQueue
     * @param downloadQueue
     */
    public void setQueue(DownloadQueue downloadQueue){
        queue=downloadQueue;
    }

    /**
     * add a new DownloadInfo to queue of Program
     * @param downloadInfo
     */
    public void addDownloadInQueue(DownloadInfo downloadInfo){
        queue.addDownload(downloadInfo);
    }

    /**
     * Getter for WithoutQueue
     * @return withoutQueue
     */
    public ArrayList<DownloadInfo> getWithoutQueue() {
        return withoutQueue;
    }

    /**
     * Getter for numberOfDownload
     * @return numberOfDownload
     */
    public int getNumberOfDownload() {
        return numberOfDownload;
    }

    /**
     * Setter for numberOfDownload
     * @param numberOfDownload
     */
    public void setNumberOfDownload(int numberOfDownload) {
        this.numberOfDownload = numberOfDownload;
    }

    /**
     * Setter for address
     * @param address
     */
    public void setAddress(String address){
        this.address=address;
    }

    /**
     * Getter for address
     * @return address
     */
    public String getAddress(){
        return address;
    }

    /**
     * Getter for queue
     * @return queue
     */
    public DownloadQueue getQueue() {
        return queue;
    }

    /**
     * getter for completed
     * @return completed
     */
    public ArrayList<DownloadInfo> getCompleted() {
        return completed;
    }

    /**
     * This method says that we have limiting about multi downloading or no
     * @return
     */
    public boolean hasLimitDownload(){
        int numRunning=downloads.size();
        if(numRunning<numberOfDownload){
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * This method gives a downloadInfo and add to related arrayList
     *
     * @param downloadInfo new Download Info
     * @param inQueue download should enter queue of Download Manager or not
     */
    public void newDownload(DownloadInfo downloadInfo,boolean inQueue){
        if(hasLimitDownload()){
            limited.add(downloadInfo);
            limitedStateQueue.add(inQueue);
            return;
        }
        downloads.add(downloadInfo);
        changed=true;
        if(inQueue==false){
            withoutQueue.add(downloadInfo);
            downloadInfo.getDownloadRun().execute();
        }
        else{
            if(queue==null){
                DownloadQueue downloadQueue=new DownloadQueue("ProgramQueue");
                setQueue(downloadQueue);
            }
            addDownloadInQueue(downloadInfo);
            queueManage();
        }
    }

    /**
     * when a download completed or removed we can
     * start a download that been in line
     */
    public void enterLimitedDownload(){
        if(limited.size()==0){
            return;
        }
        newDownload(limited.get(0),limitedStateQueue.get(0));
        limited.remove(0);
        limitedStateQueue.remove(0);
    }

    /**
     * when a downloadInfo completes
     * @param downloadInfo completed downloadInfo
     */
    public void downloadCompleted(DownloadInfo downloadInfo){
        downloads.remove(downloadInfo);
        queue.getDownloadInfos().remove(downloadInfo);
        completed.add(downloadInfo);
        queueManage();
        changed=true;

        enterLimitedDownload();
    }

    /**
     * getter for changed
     * @return changed
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * setter for changed
     * @param changed
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    /**
     * This method request to Download Manager
     * that run top of download in queue
     * and pause other downloads in queue
     */
    public void queueManage(){
        if(queue==null || queue.getDownloadInfos().size()==0){
            return;
        }
        else{
            if(queue.getDownloadInfos().get(0).getDownloadRun().isCancelled()){
                withoutQueue.add( queue.getDownloadInfos().remove(0));
                queueManage();
            }
            for(DownloadInfo downloadInfo:queue.getDownloadInfos()){
                downloadInfo.getDownloadRun().setPauseQueue(true);
            }
            if(queue.getDownloadInfos().size()>0){
                queue.getDownloadInfos().get(0).getDownloadRun().execute();
                queue.getDownloadInfos().get(0).getDownloadRun().resumeQueue();
            }
        }
    }

    /**
     * Getter for downloads
     * @return downloads
     */
    public ArrayList<DownloadInfo> getDownloads() {
        return downloads;
    }

    /**
     * Sorting downloads
     * @param timeSort if time should sort or no
     * @param nameSort if name should sort or no
     * @param sizeSort if size should sort or no
     * @param increase sort increase or decrees
     */
    public void sort(boolean timeSort,boolean nameSort,boolean sizeSort,boolean increase){
        downloadSortTime=timeSort;
        downloadSortName=nameSort;
        downloadSortSize=sizeSort;
        downloadSortIncrease=increase;

        if(timeSort){
            sortTime();
        }
        if(nameSort){
            sortName(timeSort);
        }
        if(sizeSort){
            sortSize(timeSort,nameSort);
        }
        if(increase==false){
            reverseDownloads();
        }
    }

    /**
     * This method sort downloads with default
     */
    public void sort(){
        sort(downloadSortTime,downloadSortName,downloadSortSize,downloadSortIncrease);
    }

    /**
     * Reverser for downloads
     */
    public void reverseDownloads(){
        ArrayList<DownloadInfo> newQueue=new ArrayList<>();
        while(downloads.isEmpty()==false){
            newQueue.add(downloads.get(downloads.size()-1));
            downloads.remove(downloads.size()-1);
        }
        downloads=newQueue;
    }

    /**
     * Sorter by size
     * @param isSortTime if before downloads sorted by time or no
     * @param isSortName if before downloads sorted by name or no
     */
    public void sortSize(boolean isSortTime,boolean isSortName){
        ArrayList<DownloadInfo> newQueue=new ArrayList<>();
        int numOfDownloads=downloads.size();
        for(int i=0 ; i<numOfDownloads ; i++){
            DownloadInfo downloadInfoBest=downloads.get(0);
            for(int j=1 ; j<downloads.size() ; j++){
                DownloadInfo downloadInfoNew=downloads.get(j);
                if(isSortTime==false && isSortName==false &&
                        downloadInfoNew.getSizeOfFile()<(downloadInfoBest.getSizeOfFile())){
                    downloadInfoBest=downloadInfoNew;
                }
                else if(isSortTime==true && isSortName==false &&
                        downloadInfoNew.getTimeOfStart().compareTo(downloadInfoBest.getTimeOfStart())==0){
                    if(downloadInfoNew.getSizeOfFile()<downloadInfoBest.getSizeOfFile()){
                        downloadInfoBest=downloadInfoNew;
                    }
                }else if(isSortTime==false && isSortName==true &&
                        downloadInfoNew.getName().compareTo(downloadInfoBest.getName())==0){
                    if(downloadInfoNew.getSizeOfFile()<downloadInfoBest.getSizeOfFile()){
                        downloadInfoBest=downloadInfoNew;
                    }
                }
                else if(isSortTime==true && isSortName==true){
                    if(downloadInfoNew.getTimeOfStart().compareTo(downloadInfoBest.getTimeOfStart())==0
                            &&downloadInfoNew.getName().compareTo(downloadInfoBest.getName())==0){
                        downloadInfoBest=downloadInfoNew;
                    }
                }

            }
            newQueue.add(downloadInfoBest);
            downloads.remove(downloadInfoBest);
        }
        downloads=newQueue;
    }

    /**
     * Sorter by name
     * @param isSortTime if before downloads sorted by time or no
     */
    public void sortName(boolean isSortTime){
        ArrayList<DownloadInfo> newQueue=new ArrayList<>();
        int numOfDownloads=downloads.size();
        for(int i=0 ; i<numOfDownloads ; i++){
            DownloadInfo downloadInfoBest=downloads.get(0);
            for(int j=1 ; j<downloads.size() ; j++){
                DownloadInfo downloadInfoNew=downloads.get(j);
                if(isSortTime==false && downloadInfoNew.getName().compareTo(downloadInfoBest.getName())<0){
                    downloadInfoBest=downloadInfoNew;
                }
                else if(isSortTime==true && downloadInfoNew.getTimeOfStart().compareTo(downloadInfoBest.getTimeOfStart())==0){
                    if(downloadInfoNew.getName().compareTo(downloadInfoBest.getName())<0){
                        downloadInfoBest=downloadInfoNew;
                    }
                }
            }
            newQueue.add(downloadInfoBest);
            downloads.remove(downloadInfoBest);
        }
        downloads=newQueue;
    }


    /**
     * Sorter by time
     */
    public void sortTime(){
        ArrayList<DownloadInfo> newQueue=new ArrayList<>();
        int numOfDownloads=downloads.size();
        for(int i=0 ; i<numOfDownloads ; i++){
            DownloadInfo downloadInfoBest=downloads.get(0);
            for(int j=1 ; j<downloads.size() ; j++){
                DownloadInfo downloadInfoNew=downloads.get(j);
                if(downloadInfoNew.getTimeOfStart().compareTo(downloadInfoBest.getTimeOfStart())<0){
                    downloadInfoBest=downloadInfoNew;
                }
            }
            newQueue.add(downloadInfoBest);
            downloads.remove(downloadInfoBest);
        }
        downloads=newQueue;
    }


    /**
     * Write two file about queue and downloads
     */
    public void writeDownloadsFile(){
        writeDownloadFileList();
        writeDownloadFileQueue();
    }

    /**
     * write list of download on list.jdm
     */
    public void writeDownloadFileList(){
        File txtFile = new File("list.jdm");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(txtFile));
            for (DownloadInfo downloadInfo:downloads){
                writeDetails(writer,downloadInfo);
            }
            for (DownloadInfo downloadInfo:completed){
                writeDetails(writer,downloadInfo);
            }

            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException ex) {
                ex.printStackTrace(System.err);
            }
        }
    }

    /**
     * Write downloads in queue on queue.jdm
     */
    public void writeDownloadFileQueue(){
        File txtFile = new File("queue.jdm");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(txtFile));
            writer.write(queue.getName()+"\r\n");
            writer.write(queue.getDownloadInfos().size()+"\r\n");
            for(DownloadInfo downloadInfo:queue.getDownloadInfos()){
                writer.write(downloadInfo.toString()+"\r\n");
            }
            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException ex) {
                ex.printStackTrace(System.err);
            }
        }


    }

    /**
     * This method write dates in downloadInfo with input writer
     * @param writer writer of file
     * @param downloadInfo
     * @throws IOException if cant do job throw IOException
     */
    public void writeDetails(BufferedWriter writer,DownloadInfo downloadInfo) throws IOException {
        writer.write(downloadInfo.getName()+"\r\n");
        writer.write(downloadInfo.getAddressOfFile()+"\r\n");
        writer.write(downloadInfo.getAddressOfLink()+"\r\n");
        writer.write(downloadInfo.getTimeOfStart()+"\r\n");
        writer.write(downloadInfo.getSizeOfFile()+"\r\n");
        writer.write(downloadInfo.isCompleted()+"\r\n");
    }

    /**
     * This method read dates of downloadInfo and make this and returns new downloadInfo
     * @param reader reader of file
     * @return new DownloadInfo
     * @throws IOException if cant do job throw IOException
     */
    public DownloadInfo readDetails(BufferedReader reader) throws IOException{
        String newName = reader.readLine();
        String newAddressOfFile = reader.readLine();
        String newAddressOfLink = reader.readLine();
        String newTimeOfStart = reader.readLine();
        String newSizeOfFile = reader.readLine();
        String status=reader.readLine();

        return new DownloadInfo(Long.valueOf(newSizeOfFile),Boolean.valueOf(status),
                newAddressOfFile,newAddressOfLink,newTimeOfStart,newName,this);
    }

    /**
     * update without Queue
     */
    public void updateWithoutQueue(){
        withoutQueue.clear();
        for(DownloadInfo downloadInfo:downloads){
            if(!queue.getDownloadInfos().contains(downloadInfo)){
                withoutQueue.add(downloadInfo);
            }
        }
    }

    /**
     * Read two file about queue and downloads (if these files exists)
     */
    public void readDownloadFile(){
        readDownloadFileList();
        readDownloadFileQueue();
        updateWithoutQueue();
    }

    /**
     * Read downloads of file list.jdm
     */
    public void readDownloadFileList(){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("list.jdm"));
            DownloadInfo newDownloadInfo;


            while (reader.ready()) {
                newDownloadInfo=readDetails(reader);

                if(newDownloadInfo.isCompleted()){
                    completed.add(newDownloadInfo);
                }
                else{
                    downloads.add(newDownloadInfo);
                }
            }
        } catch (IOException ex) {
            System.out.println("list.jdm not exist before");
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }

    }

    /**
     * Read queue of file queue.jdm
     */
    public void readDownloadFileQueue(){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("queue.jdm"));
            while (reader.ready()) {
                DownloadQueue newDownloadQueue;
                String nameOfQueue;
                int numberOfDownloads;
                nameOfQueue = reader.readLine();
                newDownloadQueue=new DownloadQueue(nameOfQueue);
                numberOfDownloads=Integer.parseInt(reader.readLine());
                for(int i=1 ; i<=numberOfDownloads ; i++){
                    String line=reader.readLine();
                    for(DownloadInfo downloadInfo:downloads){
                        if(downloadInfo.toString().equals(line)){
                            newDownloadQueue.addDownload(downloadInfo);
                            break;
                        }
                    }
                }
                setQueue(newDownloadQueue);
            }
        } catch (IOException ex) {
            System.out.println("queue.jdm not exist before");
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }

    /**
     * This method read filter url of filter.jdm
     */
    public void readFileFilter(){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("filter.jdm"));
            filterLink=new ArrayList<>();
           while (reader.ready()) {
                String newFilter;
                newFilter=reader.readLine();
                filterLink.add(newFilter);
            }
        } catch (IOException ex) {
            System.out.println("filter.jdm not exist before;We have not filter url :)");

        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }

    /**
     * This method gives a url ans says this url is filter or no
     * @param url input url
     * @return result of filtering
     */
    public boolean isFilter(String url){
        for(String string:filterLink){
            if(string.indexOf('/')!=-1 && string.lastIndexOf('/')-1!=string.indexOf('/')){
                if(string.equals(url)){
                    return true;
                }
//                System.out.println(string+" --> file");
            }
            else{
//                System.out.println(string+" --> Site");

                String str=string.replace("http://","");
                str=str.replace("https://","");
                str=str.replace("www.","");
//                System.out.println("Removed: "+str);

                if(url.contains(str)){
                    return true;
                }

            }
        }
        return false;
    }

    /**
     * when removed a DownloadInfo write in removed.jdm
     * @param newRemoveDownloadInfo a new DownloadInfo that removed
     */
    public void removeDownload(DownloadInfo newRemoveDownloadInfo){
        ArrayList<DownloadInfo> removedDownloadInfo=new ArrayList<>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("removed.jdm"));
            while (reader.ready()) {
                DownloadInfo newDownloadInfo=readDetails(reader);
                removedDownloadInfo.add(newDownloadInfo);
            }
        } catch (IOException ex) {
            System.out.println("removed.jdm not exist before");
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
        removedDownloadInfo.add(newRemoveDownloadInfo);
        downloads.remove(newRemoveDownloadInfo);
        completed.remove(newRemoveDownloadInfo);
        queue.getDownloadInfos().remove(newRemoveDownloadInfo);
        queueManage();

        File txtFile = new File("removed.jdm");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(txtFile));
            for (DownloadInfo downloadInfo:removedDownloadInfo){
                writeDetails(writer,downloadInfo);
            }
            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException ex) {
                ex.printStackTrace(System.err);
            }
        }

        enterLimitedDownload();
    }

    /**
     * If downloads sorted by time or no
     * @return
     */
    public boolean isDownloadSortTime() {
        return downloadSortTime;
    }

    /**
     * If downloads sorted by name or no
     * @return
     */
    public boolean isDownloadSortName() {
        return downloadSortName;
    }

    /**
     * If downloads sorted by size or no
     * @return
     */
    public boolean isDownloadSortSize() {
        return downloadSortSize;
    }

    /**
     * If downloads sorted increase or no
     * @return
     */
    public boolean isDownloadSortIncrease() {
        return downloadSortIncrease;
    }

    /**
     * This method execute DownloadRun of DownloadInfo
     * and if DownloadRun canceled, makes a new DownloadRun
     * @param downloadInfo
     */
    public void runDownload(DownloadInfo downloadInfo){
        downloadInfo.getDownloadRun().execute();
        if(downloadInfo.getDownloadRun().isCancelled()){
            downloadInfo.reMakeDownloadRun();
            downloadInfo.getDownloadRun().execute();
        }
    }
}
