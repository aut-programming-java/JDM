import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Queue Frame is a frame for changing order of queue
 * or add and remove downloads to queue
 *
 * @author Alireza Mazochi
 * @since 1397-02-28
 * @version 1.0.0
 */
public class QueueFrame extends JFrame {
    private JTabbedPane tabbedPane;
    private JPanel addPanel;
    private JPanel orderPanel;
    private DownloadManager downloadManager;
    private JPanel downloadPartAdd[];
    private PreferredButton buttonAddRemove[];

    private PreferredButton buttonGoTop[];
    private JPanel downloadPartOrder[];

    private int nowChoiceTab;

    private MainFrame mainFrame;
    /**
     * Constructor for QueueFrame
     *
     * @param downloadManager DownloadManager of my run app
     */
    public QueueFrame(DownloadManager downloadManager,MainFrame mainFrame){
        super("Change Queue");
        this.downloadManager=downloadManager;
        this.mainFrame=mainFrame;
        nowChoiceTab=0;
        paintTotal();

    }

    /**
     * This method is a part of constructor
     */
    public void paintTotal(){
        JPanel contentPanel=new JPanel();
        setContentPane(contentPanel);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
        setResizable(false);
        addPanel=new JPanel();
        orderPanel=new JPanel();

        setSize(new Dimension(1000,100+50*downloadManager.getDownloads().size()));




        makeAddPanel();
        makeOrderPanel();

        tabbedPane=new JTabbedPane();
        tabbedPane.add("Add & Remove",addPanel);
        tabbedPane.add("Change Order",orderPanel);
        add(tabbedPane,BorderLayout.CENTER);
        setVisible(true);
        tabbedPane.setSelectedIndex(nowChoiceTab);
//        pack();
    }

    /**
     * This method make panel relate to changing order
     */
    public void makeOrderPanel(){
        int numOfDownloads=downloadManager.getDownloads().size();
        downloadPartOrder=new JPanel[numOfDownloads];
        buttonGoTop=new PreferredButton[numOfDownloads];

        orderPanel.setLayout(new GridLayout(numOfDownloads,1));
        MyActionListener2 actionListener2=new MyActionListener2();

        int cont=0;
        for(DownloadInfo downloadInfo:downloadManager.getQueue().getDownloadInfos()){
            buttonGoTop[cont]=new PreferredButton("Go Top");
            downloadPartOrder[cont]=new JPanel();
            downloadPartOrder[cont].setLayout(new GridLayout(1,3));
            downloadPartOrder[cont].setSize(1000,50);

            if(downloadInfo.getName().length()>20){
                downloadPartOrder[cont].add(new JLabel(downloadInfo.getName().substring(0,20)+"..."));
            }
            else{
                downloadPartOrder[cont].add(new JLabel(downloadInfo.getName()));
            }
            if(downloadInfo.getAddressOfLink().length()>20){
                downloadPartOrder[cont].add(new JLabel(downloadInfo.getAddressOfLink().substring(0,20)+"..."));
            }
            else {
                downloadPartOrder[cont].add(new JLabel(downloadInfo.getAddressOfLink()));
            }
            downloadPartOrder[cont].add(buttonGoTop[cont]);
            buttonGoTop[cont].addActionListener(actionListener2);

            orderPanel.add(downloadPartOrder[cont]);
            cont++;
        }

    }

    /**
     * This method make panel relate to being or not being downloads in queue
     */
    public void makeAddPanel(){

        int numOfDownloads=downloadManager.getDownloads().size();
        buttonAddRemove=new PreferredButton[numOfDownloads];
        downloadPartAdd=new JPanel[numOfDownloads];


        addPanel.setLayout(new GridLayout(numOfDownloads,1));
        JPanel titlePanel=new JPanel();
//        titlePanel.setLayout(new GridLayout(1,3));
//        titlePanel.add(new JLabel("Name"));
//        titlePanel.add(new JLabel("Address"));
//        titlePanel.add(new JLabel("Operate"));
//        addPanel.add(titlePanel);

        MyActionListener actionListener=new MyActionListener();
        int cont=0;
        for(DownloadInfo downloadInfo:downloadManager.getDownloads()){
            if(downloadManager.getWithoutQueue().contains(downloadInfo)){
                buttonAddRemove[cont]=new PreferredButton("Add");
            }
            else{
                buttonAddRemove[cont]=new PreferredButton("Remove");
            }
            downloadPartAdd[cont]=new JPanel();
            downloadPartAdd[cont].setLayout(new GridLayout(1,3));
            downloadPartAdd[cont].setSize(1000,50);
            if(downloadInfo.getName().length()>20){
                downloadPartAdd[cont].add(new JLabel(downloadInfo.getName().substring(0,20)+"..."));
            }
            else{
                downloadPartAdd[cont].add(new JLabel(downloadInfo.getName()));
            }
            if(downloadInfo.getAddressOfLink().length()>20){
                downloadPartAdd[cont].add(new JLabel(downloadInfo.getAddressOfLink().substring(0,20)+"..."));
            }
            else{
                downloadPartAdd[cont].add(new JLabel(downloadInfo.getAddressOfLink()));
            }
            downloadPartAdd[cont].add(buttonAddRemove[cont]);
            addPanel.add(downloadPartAdd[cont]);
            buttonAddRemove[cont].addActionListener(actionListener);
            cont++;
        }

    }

    /**
     * This private class belongs to buttons of AddPanel
     * used for adding or removing a download to queue
     */
    private class MyActionListener implements ActionListener{
        /**
         * Invoked when an action occurs.
         *
         * @param e
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            PreferredButton button=(PreferredButton)e.getSource();
            for(int i=0 ; i<downloadManager.getDownloads().size() ; i++){
                if(button.equals(buttonAddRemove[i])){
                    if(downloadManager.getWithoutQueue().contains(downloadManager.getDownloads().get(i))){
                        buttonAddRemove[i].setText("Remove");
                        downloadManager.addDownloadInQueue(downloadManager.getDownloads().get(i));
                        downloadManager.getWithoutQueue().remove(downloadManager.getDownloads().get(i));
                    }
                    else{
                        buttonAddRemove[i].setText("Add");
                        downloadManager.getQueue().getDownloadInfos().remove(downloadManager.getDownloads().get(i));
                        downloadManager.getWithoutQueue().add(downloadManager.getDownloads().get(i));
                        downloadManager.getDownloads().get(i).getDownloadRun().execute();
                        downloadManager.getDownloads().get(i).getDownloadRun().resume();
                    }
                    downloadManager.queueManage();

                }
            }
            downloadManager.writeDownloadsFile();
            nowChoiceTab=0;
            paintTotal();
            mainFrame.updateChanel();
        }
    }

    /**
     * This private class belongs to orderPanel
     * Used for change order of a download in queue
     */
    private class MyActionListener2 implements ActionListener{

        /**
         * Invoked when an action occurs.
         *
         * @param e
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            PreferredButton button=(PreferredButton)e.getSource();
            for(int i=0 ; i<downloadManager.getQueue().getDownloadInfos().size() ; i++){
                if(button.equals(buttonGoTop[i])){
                    DownloadQueue newDownloadQueue=new DownloadQueue("Queue");
                    newDownloadQueue.addDownload(downloadManager.getQueue().getDownloadInfos().get(i));
                    for(int j=0 ; j<downloadManager.getQueue().getDownloadInfos().size() ; j++){
                        if(j!=i){
                            newDownloadQueue.addDownload(downloadManager.getQueue().getDownloadInfos().get(j));
                        }
                    }
                    downloadManager.setQueue(newDownloadQueue);
                    downloadManager.queueManage();
                }

            }
            downloadManager.writeDownloadsFile();
            nowChoiceTab=1;
            paintTotal();
            mainFrame.updateChanel();
        }
    }
}
