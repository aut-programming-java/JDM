import javax.swing.*;
import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * This class make a zip file of all file that makes by application
 *
 * @author Alireza Mazochi
 * @since 1397-03-11
 * @version 1.0.0
 */
public class ExportZip {
    StringBuilder sb;
    ZipOutputStream out;

    /**
     * This method make a zip file of all file that makes by application
     */
    public void makeZip()  {
        File f = new File("JDM.zip");
        out = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            readStringFile("setting.jdm");
            writeStringFile("setting.jdm");
        } catch (IOException e) {
            System.out.println("We could not find setting.jdm");
        }


        try {
            readStringFile("list.jdm");
            writeStringFile("list.jdm");
        } catch (IOException e) {
            System.out.println("We could not find list.jdm");

        }

        try {
            readStringFile("removed.jdm");
            writeStringFile("removed.jdm");
        } catch (IOException e) {
            System.out.println("We could not find removed.jdm");
        }

        try {
            readStringFile("queue.jdm");
            writeStringFile("queue.jdm");

        } catch (IOException e) {
            System.out.println("We could not find queue.jdm");
        }

        try {
            readStringFile("filter.jdm");
            writeStringFile("filter.jdm");
        } catch (IOException e) {
            System.out.println("We could not find filter.jdm");
        }


        try {
            out.close();
        } catch (IOException e1) {
            e1.printStackTrace();

        }
        JOptionPane.showMessageDialog(null,"Export completed!","Exporting",JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     *  read string of fileName
     * @param fileName
     * @throws IOException
     */
    public void readStringFile(String fileName) throws IOException{
        sb = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
            while (reader.ready()) {
                sb.append(reader.readLine()+"\r\n");
            }
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException ex) {
                System.err.println(ex);

            }
        }
    }

    /**
     * write string to a fileName in zip file
     * @param fileName
     */
    public void writeStringFile(String fileName){
        ZipEntry e = new ZipEntry(fileName);

        try {
            out.putNextEntry(e);
            byte[] data = sb.toString().getBytes();
            out.write(data, 0, data.length);
            out.closeEntry();

        } catch (IOException e5) {
            e5.printStackTrace();

        }

    }
}
