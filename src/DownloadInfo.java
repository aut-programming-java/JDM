import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * This class is details of a download
 * and for showing needs to a object of class DownloadInfoPanel
 *
 * @see DownloadInfoPanel
 * @author Alireza Mazochi
 * @since 1397-02-28
 * @version 1.0.0
 */

public class DownloadInfo {
    private long speedOfDownload;
    private long sizeOfFile;
    private long sizeOfDownloaded;
    private String addressOfFile;
    private String addressOfLink;
    private String timeOfStart;
    private String name;
    private String progressPercent;

    private boolean status;

    private DownloadRun downloadRun;
    private DownloadManager downloadManager;
    /**
     * Constructor for DownloadInfo
     * This constructor used when in run application make a new DownloadInfo
     * @param name name of download
     * @param sizeOfFile size of download
     * @param addressOfFile address of location saving
     * @param addressOfLink link of download
     * @param downloadManager downloadManager
     */
    public DownloadInfo(String name,long sizeOfFile,String addressOfFile,String addressOfLink,DownloadManager downloadManager){
        this.name=name;
        this.sizeOfFile=sizeOfFile;
        this.addressOfFile=addressOfFile;
        this.addressOfLink=addressOfLink;
        this.timeOfStart= new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
        this.downloadManager=downloadManager;

        sizeOfDownloaded=0;
        speedOfDownload=0;
        status=false;
        progressPercent="0";

        downloadRun=new DownloadRun(this,downloadManager);
    }

    /**
     * Constructor for DownloadInfo
     * This constructor used when previous DownloadInfo read of file
     *
     * @param sizeOfFile size of download
     * @param status state of download
     * @param addressOfFile address of location saving
     * @param addressOfLink link of download
     * @param timeOfStart time that download started
     * @param name name of download
     * @param downloadManager downloadManager
     */
    public DownloadInfo(long sizeOfFile,boolean status, String addressOfFile,
                        String addressOfLink, String timeOfStart, String name,DownloadManager downloadManager) {
        this.sizeOfFile = sizeOfFile;
        this.addressOfFile = addressOfFile;
        this.addressOfLink = addressOfLink;
        this.timeOfStart = timeOfStart;
        this.name = name;
        this.downloadManager=downloadManager;

        this.status=status;

        downloadRun=new DownloadRun(this,downloadManager);

        if(status){
            progressPercent="100";
            this.sizeOfDownloaded=sizeOfFile;
        }
        else{
            progressPercent="0";
            this.sizeOfDownloaded=0;

        }
    }

    /**
     * This method when called downloadInfo completed
     */
    public void completed(){
        status=true;
        downloadManager.downloadCompleted(this);
    }

    /**
     * getter for status
     * @return status
     */
    public boolean isCompleted(){
        return status;
    }

    /**
     * Getter for progressPercent
     * @return progressPercent
     */
    public String getProgressPercent() {
        return progressPercent;
    }

    /**
     * Getter for sizeOfFile
     * @return sizeOfFile
     */
    public long getSizeOfFile() {
        return sizeOfFile;
    }

    /**
     * Getter for sizeOfDownloaded
     * @return sizeOfDownloaded
     */
    public long getSizeOfDownloaded() {
        return sizeOfDownloaded;
    }

    /**
     * Getter for addressOfFile
     * @return addressOfFile
     */
    public String getAddressOfFile() {
        return addressOfFile;
    }

    /**
     * Getter for addressOfLink
     * @return addressOfLink
     */
    public String getAddressOfLink() {
        return addressOfLink;
    }

    /**
     * Getter for timeOfStart
     * @return timeOfStart
     */
    public String getTimeOfStart() {
        return timeOfStart;
    }

    /**
     * Getter for speedOfDownload
     * @return speedOfDownload
     */
    public long getSpeedOfDownload() {
        return speedOfDownload;
    }

    /**
     * Getter for name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setter for progressPercent
     * @param progressPercent
     */
    public void setProgressPercent(String progressPercent) {
        this.progressPercent = progressPercent;
    }

    /**
     * getter for downloadRun
     * @return downloadRun
     */
    public DownloadRun getDownloadRun() {
        return downloadRun;
    }

    /**
     * setter for speedOfDownload
     * @param speedOfDownload
     */
    public void setSpeedOfDownload(long speedOfDownload) {
        this.speedOfDownload = speedOfDownload;
    }

    /**
     * setter for sizeOfDownloaded
     * @param sizeOfDownloaded
     */
    public void setSizeOfDownloaded(long sizeOfDownloaded) {
        this.sizeOfDownloaded = sizeOfDownloaded;
    }

    /**
     * when a download failed and canceled
     * we can use this method for remaking DownloadRun
     */
    public void reMakeDownloadRun(){
        sizeOfDownloaded=0;
        speedOfDownload=0;
        progressPercent="0";
        downloadRun=new DownloadRun(this,downloadManager);
    }


    /**
     * This method override class Object
     * used for writing in file
     * @return string that contains details of download
     */
    public String toString(){
        StringBuilder string=new StringBuilder("");
        string.append(name);
        string.append("###");
        string.append(sizeOfFile);
        string.append("###");
        string.append(addressOfFile);
        string.append("###");
        string.append(addressOfLink);
        string.append("###");
        string.append(timeOfStart);
        string.append("###");
        string.append(status);
        string.append("###");
        return string.toString();
    }
}
