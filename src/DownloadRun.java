import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class can download and get files of network
 * And change info of InfoDownload like sizeOfDownloaded or speed in part of second!
 *
 * @author Alireza Mazochi
 * @author www.codejava.net
 * @since 1397-03-11
 * @version 1.0.0
 */
public class DownloadRun extends SwingWorker<Object,Object> {
    private DownloadInfo downloadInfo;
    private DownloadManager downloadManager;
    private boolean pause;

    private String downloadURL;
    private String saveDirectory;
    private static final int BUFFER_SIZE = 4096;

    /**
     * Constructor for DownloadRun
     * @param downloadInfo info of a download
     * @param downloadManager downloadManager of my program
     */
    public DownloadRun(DownloadInfo downloadInfo,DownloadManager downloadManager){
        this.downloadInfo=downloadInfo;
        downloadURL=downloadInfo.getAddressOfLink();
        saveDirectory=downloadInfo.getAddressOfFile();
        this.downloadManager=downloadManager;
        pause=false;
    }


    /**
     * Computes a result, or throws an exception if unable to do so.
     * <p>
     * <p>
     * Note that this method is executed only once.
     * <p>
     * <p>
     * Note: this method is executed in a background thread.
     *
     * @return the computed result
     * //@throws Exception if unable to compute a result
     */
    @Override
    protected Object doInBackground(){
        try {
            DownloadUtil util = new DownloadUtil();
            util.downloadFile(downloadURL);
            //show first details
//            System.out.println(util.getFileName());
//            System.out.println(util.getContentLength());

            String saveFilePath = saveDirectory + File.separator + util.getFileName();

            InputStream inputStream = util.getInputStream();
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            long totalBytesRead = 0;
            int percentCompleted = 0;
            long fileSize = util.getContentLength();
            long start=System.nanoTime();
            long byteOfStart=0;
            while ((bytesRead = inputStream.read(buffer)) != -1) {

                while (pause){
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                    }
                    start=System.nanoTime();
                    byteOfStart=bytesRead;
                }


                if(isCancelled()){
                    downloadInfo.setSpeedOfDownload(0);
                    downloadInfo.setSizeOfDownloaded(0);
                    downloadInfo.setProgressPercent("0");
                    break;
                }

                long end=System.nanoTime();
                long time=end-start;

                if(time>75*1000*1000){
                    long speed=(totalBytesRead-byteOfStart)*1l*1000*1000*1000/time;
                    downloadInfo.setSpeedOfDownload(speed);

                    start=System.nanoTime();
                    byteOfStart=totalBytesRead;
                }

                outputStream.write(buffer, 0, bytesRead);
                totalBytesRead += bytesRead;
                percentCompleted = (int) (totalBytesRead * 100 / fileSize);

                downloadInfo.setProgressPercent(percentCompleted+"");
                downloadInfo.setSizeOfDownloaded(totalBytesRead);
            }

            outputStream.close();
            util.disconnect();
        }
        catch (IOException ex){
            JOptionPane.showMessageDialog(null, "Error downloading file: " + ex.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
            downloadInfo.setSpeedOfDownload(0);
            downloadInfo.setSizeOfDownloaded(0);
            downloadInfo.setProgressPercent("0");
            cancel(true);
        }
        return null;
    }

    /**
     * when a download complete or canceled
     */
    protected void done(){
        if (!isCancelled()) {
            downloadInfo.completed();
        }
        else{
            if(downloadManager.getQueue().getDownloadInfos().contains(downloadInfo)){
                downloadManager.getQueue().getDownloadInfos().remove(downloadInfo);
                downloadManager.getWithoutQueue().add(downloadInfo);
                downloadManager.queueManage();
                downloadManager.setChanged(true);
            }
        }
    }

    /**
     * This method resume a download if paused
     * This method does not worked for download in queue
     */
    public void resume(){
        if(!downloadManager.getWithoutQueue().contains(downloadInfo)){
            System.out.println("Resuming only apply to withoutQueue downloads");
            return;
        }
        if(isPause() && !isDone()){
            pause=false;
            firePropertyChange("pause",true,false);
        }
    }

    /**
     * This method resume a download if paused
     * This method only for queue
     * And Only queue can resume download in queue
     */
    public void resumeQueue(){
        if(isPause() && !isDone()){
            pause=false;
            firePropertyChange("pause",true,false);
        }
    }

    /**
     * getter for pause
     * @return pause
     */
    public boolean isPause() {
        return pause;
    }

    /**
     * This method pause a download
     * This method does not worked for download in queue
     */
    public void setPause(boolean pause) {
        if(!downloadManager.getWithoutQueue().contains(downloadInfo)){
            System.out.println("pausing only apply to withoutQueue downloads");
            return;
        }
        this.pause = pause;
    }

    /**
     * This method pause a download
     * This method only for queue
     * And Only queue can pause download in queue
     */
    public void setPauseQueue(boolean pause){
        this.pause = pause;

    }

}
