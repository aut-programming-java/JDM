import javax.swing.*;

/**
 * This application for managing download
 * This application look like EagleGet
 * but very completer than it :)
 *
 * This code relate to my Midterm Project
 *
 * @author Alireza Mazochi
 * @since 1397-02-28
 * @version 1.0.0
 */


public class Main {
    /**
     * This method is main and starting point for program
     * @param args string that enter in command line
     */
    public static  void main(String args[]){
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }

        DownloadManager downloadManager=new DownloadManager();
        MainFrame mainFrame=new MainFrame(downloadManager);
        mainFrame.showGUT();
        mainFrame.updateDownloads();
        GUIUpdater updater=new GUIUpdater(mainFrame,downloadManager);
        updater.execute();

    }

}

